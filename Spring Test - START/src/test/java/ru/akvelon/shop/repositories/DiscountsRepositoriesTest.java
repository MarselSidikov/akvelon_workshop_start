package ru.akvelon.shop.repositories;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import ru.akvelon.shop.dto.DiscountForPriceDto;
import ru.akvelon.shop.dto.DiscountsForPriceDto;
import ru.akvelon.shop.dto.DiscountsForPricesDto;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.Collections;

public class DiscountsRepositoriesTest {

    private DiscountsRepositoryJdbc discountsRepositoryJdbc;

    public static DiscountsForPricesDto expectedPrices() {
        return DiscountsForPricesDto.builder()
                .data(Arrays.asList(
                        DiscountsForPriceDto.builder()
                                .price(100.0)
                                .discounts(Arrays.asList(
                                        DiscountForPriceDto.builder()
                                                .percents(15.0)
                                                .priceByDiscount(85.0)
                                                .build(),
                                        DiscountForPriceDto.builder()
                                                .percents(20.0)
                                                .priceByDiscount(80.0)
                                                .build()))
                                .build(),
                        DiscountsForPriceDto.builder()
                                .price(200.0)
                                .discounts(Arrays.asList(
                                        DiscountForPriceDto.builder()
                                                .percents(15.0)
                                                .priceByDiscount(170.0)
                                                .build(),
                                        DiscountForPriceDto.builder()
                                                .percents(20.0)
                                                .priceByDiscount(160.0)
                                                .build()))
                                .build(),
                        DiscountsForPriceDto.builder()
                                .price(300.0)
                                .discounts(Arrays.asList(
                                        DiscountForPriceDto.builder()
                                                .percents(15.0)
                                                .priceByDiscount(255.0)
                                                .build(),
                                        DiscountForPriceDto.builder()
                                                .percents(20.0)
                                                .priceByDiscount(240.0)
                                                .build()))
                                .build()
                ))
                .build();
    }
}
