package ru.akvelon.shop.controllers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import ru.akvelon.shop.dto.*;
import ru.akvelon.shop.exceptions.IncorrectType;
import ru.akvelon.shop.services.DiscountsService;

import java.util.Collections;

import static org.mockito.Mockito.when;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@DisplayName("DiscountsController is working when")
class DiscountControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DiscountsService discountsService;

    @BeforeEach
    public void setUp() {
        when(discountsService.getDiscountsByType("PERCENTS")).thenReturn(Collections.singletonList(
                DiscountDto.builder()
                        .description("TEST DESCRIPTION 1")
                        .value(10.0)
                        .type("PERCENTS")
                        .build()));

        when(discountsService.getDiscountsByType("BONUS")).thenReturn(Collections.singletonList(
                DiscountDto.builder()
                        .description("TEST DESCRIPTION 2")
                        .value(50.0)
                        .type("BONUS")
                        .build()));

        when(discountsService.getDiscountsByType("FAKE")).thenThrow(IncorrectType.class);

        when(discountsService.applyDiscounts(OrdersPricesDto.builder()
                .prices(Collections.singletonList(10.0))
                .build()))
                .thenReturn(DiscountsForPricesDto.builder()
                        .data(Collections.singletonList(
                                DiscountsForPriceDto.builder()
                                        .price(10.0)
                                        .discounts(Collections.singletonList(
                                                DiscountForPriceDto.builder()
                                                        .priceByDiscount(9.0)
                                                        .percents(10.0)
                                                        .build()))
                                        .build()))
                        .build());
    }

    @Test
    public void printMockDiscountsService() {
        System.out.println(discountsService.getDiscountsByType("PERCENTS"));
        System.out.println(discountsService.applyDiscounts(OrdersPricesDto
                .builder()
                .prices(Collections.singletonList(10.0))
                .build()));
    }

}