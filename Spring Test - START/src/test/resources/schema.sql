drop table if exists discount;
create table discount (
    id identity not null primary key,
    type varchar(255),
    value double precision
);