with all_prices as (select unnest(CAST('{100, 200, 300}' AS double precision[])) as value)
select price.value, (price.value - price.value / 100.0 * d.value) as by_discount, d.value as discount_value
from discount d
         cross join (select * from all_prices) as price
where d.type = 'PERCENTS'