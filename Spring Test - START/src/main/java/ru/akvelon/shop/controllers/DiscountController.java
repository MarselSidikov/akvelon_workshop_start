package ru.akvelon.shop.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.akvelon.shop.dto.DiscountDto;
import ru.akvelon.shop.dto.DiscountsForPricesDto;
import ru.akvelon.shop.dto.ExceptionDto;
import ru.akvelon.shop.dto.OrdersPricesDto;
import ru.akvelon.shop.exceptions.IncorrectPrice;
import ru.akvelon.shop.services.DiscountsService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class DiscountController {

    private final DiscountsService discountsService;

    @GetMapping("/discounts")
    public ResponseEntity<List<DiscountDto>> getDiscountsByType(@RequestParam("type") String type) {
        return ResponseEntity.ok(discountsService.getDiscountsByType(type));
    }

    @PostMapping(value = "/discounts/apply", params = "type=PERCENTS")
    public ResponseEntity<DiscountsForPricesDto> applyDiscounts(@RequestBody OrdersPricesDto ordersPrices) {
        return ResponseEntity.ok(discountsService.applyDiscounts(ordersPrices));
    }

    @ExceptionHandler(IncorrectPrice.class)
    public ResponseEntity<ExceptionDto> handleIncorrectType(IncorrectPrice exception) {
        return ResponseEntity.badRequest()
                .body(ExceptionDto.builder()
                        .code(HttpStatus.BAD_REQUEST.value())
                        .message(exception.getMessage())
                        .build());
    }
}
