package ru.akvelon.shop.repositories;

import ru.akvelon.shop.dto.DiscountsForPricesDto;

import java.util.List;

public interface DiscountsRepositoryJdbc {
    DiscountsForPricesDto applyAllDiscounts(List<Double> prices);

    Integer countOfDiscounts();
}
