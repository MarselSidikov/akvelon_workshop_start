package ru.akvelon.shop.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DiscountForPriceDto {
    private Double percents;
    private Double priceByDiscount;
}
