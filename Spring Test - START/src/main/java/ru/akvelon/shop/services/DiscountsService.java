package ru.akvelon.shop.services;

import ru.akvelon.shop.dto.DiscountDto;
import ru.akvelon.shop.dto.OrdersPricesDto;
import ru.akvelon.shop.dto.DiscountsForPricesDto;

import java.util.List;

public interface DiscountsService {
    List<DiscountDto> getDiscountsByType(String type);

    DiscountsForPricesDto applyDiscounts(OrdersPricesDto ordersPrices);
}
